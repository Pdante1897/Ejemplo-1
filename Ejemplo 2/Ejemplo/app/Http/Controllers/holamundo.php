<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use \GuzzleHttp\Client;

class holamundo extends Controller
{
    public function index(){
        Log::info("holamundo!");
        Log::error("holamundo!Error");
        $ciudades = $this->ConectarApi();
        return view('holamundo', ['ciudades' => $ciudades['listaPaises']]);
    }

    public function ConectarApi(){
        $client = new Client();
        $res = $client->request('GET', 'http://localhost:4000/paises/getCiudades');
        $respuesta = json_decode($res->getBody());
        $respuesta = json_decode(json_encode($respuesta),true );
        Log::info('---------------Respuesta----------------');
        Log::info(print_r($respuesta,true));
        Log::info('----------------------------------------');

        return $respuesta;
    }
}
