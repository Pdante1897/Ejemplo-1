const app = require ('../routes/paises.route')

const request = require ('supertest')

describe('Get /getPaises', ()=>{
    test('Tendria que responder con un estado de codigo 200 si no encuentra el endpoint', async()=>{
        const response = await request(app).get('/getPaises').send()
        console.log(response.statusCode);
        expect(response.statusCode).toBe(200);
    });
    test('Tendria que responder con un estado de codigo 404 si no encuentra el endpoint', async()=>{
        const response = await request(app).get('/getPaises/2').send()
        console.log(response.statusCode);
        expect(response.statusCode).toBe(404);
    });
});

describe('POST /postPaises', ()=>{


    let datos = {
        nombre: "Estados Unidos",
        continente: "América"
    };
    
    let paisesInconrrecto = {
        nombre2: "Estados Unidos prueba2 ",
        continente2: "America"
    }
    let paisesInconrrecto2 = {
        nombre: "Estados Unidos prueba 3 ",
    }
    let paisesInconrrecto3 = {
        continente: "America prueba 4"
    }
    //DAR FORMATO JSON PARA LAS VARIABLES
    test('Debería responder con un estado de código 200 si encuentra el endpoint', async()=>{
        const response = await request(app).post('/postPaises').send(datos);
        expect(response.statusCode).toBe(200);
    });

    test('Tendria que responder con un estado de codigo 400 si no se envia nada', async()=>{
        const response = await request(app).post('/postPaises').send()
        expect(response.statusCode).toBe(400);
    });
    test('Tendria que responder con un estado de codigo 400 si no se envia el nombre', async()=>{
        const response = await request(app).post('/postPaises').send(paisesInconrrecto)
        expect(response.statusCode).toBe(400);
    });
});