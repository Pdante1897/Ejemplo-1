
var listaPersonas = []
function insertarPersona(req, res){
    const Datos  = req.body
    if(Datos["nombre"] !== undefined && Datos["edad"] !== undefined){
        persona={
            nombre:Datos["nombre"],
            edad:Datos["edad"]
        }
        listaPersonas.push(persona)
    } else{
        return res.status(400).send({Resultado: 'Hace falta uno o mas parametros'})
    }
    return res.status(200).send({Resultado: 'ok'})

}

function obtenerPersonas(req, res){   
    return res.status(200).send({listaPersonas})
}

module.exports = {
    insertarPersona,
    obtenerPersonas
}