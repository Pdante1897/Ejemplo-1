var express = require('express')
var controller = require('../Controllers/personas.controller');
const app = express()
//endpoints
app.post('/postPersonas', controller.insertarPersona);
app.get('/getPersonas', controller.obtenerPersonas);


module.exports = app;