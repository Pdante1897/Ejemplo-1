const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();


var route_paises = require('./Routes/paises.route');
var route_personas = require('./Routes/personas.route');


app.use(bodyParser.json({limit:'50mb', extended:true}));
app.use(bodyParser.urlencoded({limit:'50mb', extended:true}));
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/paises',route_paises);
app.use('/personas',route_personas);


app.get('/', (req, res) => {
    res.send('Backend Funciona en el ejemplo de ApiRest');
})

app.listen(4000, () => {
    console.log('Corriendo en puerto 4000 :D');
});