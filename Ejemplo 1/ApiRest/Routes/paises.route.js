var express = require('express')
var controller = require('../Controllers/paises.controller');
const app = express()
//endpoints
app.post('/postPaises', controller.insertarPaises);
app.get('/getPaises', controller.obtenerPaises);
app.get('/getCiudades', controller.obtenerCiudades);

module.exports = app;