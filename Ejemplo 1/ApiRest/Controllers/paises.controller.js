var listaPaises = [];

function insertarPaises(req, res) {
    const Datos = req.body;
    
    if (!Datos) {
        return res.status(400).send({ Resultado: 'Falta el cuerpo de la petición' });
    }
    if (Datos["nombre"] && Datos["continente"]) {
        let pais = {
            nombre: Datos["nombre"],
            continente: Datos["continente"]
        };
        listaPaises.push(pais);
        return res.status(200).send({ Resultado: 'Se insertó correctamente el país' });
    } else {
        return res.status(400).send({ Resultado: 'Falta uno o más parámetros' });
    }
}

function obtenerPaises(req, res) {
    return res.status(200).send({ listaPaises });
}

function obtenerCiudades(req, res) {
    listaPaises = ['Guatemala', 'Tegucigalpa', 'San Salvador', 'Leon', 'San Juan', 'Panama'];
    return res.status(200).send({ listaPaises });
}

module.exports = {
    insertarPaises,
    obtenerPaises,
    obtenerCiudades
};