const app = require ('../routes/paises.route')
//const app2 = require ('../routes/personas.route')
const request = require ('supertest')

describe('GET /getPaises',()=>{
    test('tendria que responder con un estado de codigo 404 si no encuentra el endpoint', async()=>{
        const response = await request(app).get('/getPaises').send()
        console.log(response.statusCode);
        expect(response.statusCode).toBe(404);
    });
});


describe('POST /postPaises',()=>{
    let paises={
        "Datos": {
            "dias": 10,
            "ini_fecha": "2022-10-31",
            "fin_fecha": "2022-10-31",
            "vehiculo": 1,
            "IdUsuario": 1,
            "idUsuarioServicio": 2
        }
    }
    
    let paisesF1={
        "Datos": {
            "dias": 10,
            "ini_fecha": "2022-10-31",
            "fin_fecha": "2022-10-31",
            "vehiculo": 1
        }
    }

    test('tendria que responder con un estado de codigo 200 si encuentra el endpoint', async()=>{
        const response = await request(app).post('/rentaAuto').send(paises)
        //console.log(response);
        expect(response.statusCode).toBe(200);
    });

    test('deberia reponder con un estado de codigo 200 si el contenido es json', async ()=>{
        const response = await request(app).post('/rentaAuto').send(paises)
        //console.log(response);
        expect(response.headers['content-type']).toEqual(expect.stringContaining("json"))
    });
    
    test('tendria que responder con un estado de codigo 400 si no se envia nada', async()=>{
        const response=await request(app).post('/rentaAuto').send()
        //console.log(response.body);
        expect(response.statusCode).toBe(400);
     });
    
     test('tendria que responder con un estado de codigo 400 faltan uno o mas parametros', async()=>{
         const response=await request(app).post('/rentaAuto').send(paisesF1)
         //console.log(response.body);
         expect(response.statusCode).toBe(400);
      });
});
