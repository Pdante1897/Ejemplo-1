# Utilizamos la imagen oficial de PHP
FROM php:8.2

# Actualizamos los paquetes e instalamos las extensiones necesarias
RUN apt-get update && apt-get install -y \
    git \
    zip \
    unzip \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd pdo pdo_mysql


# Instalamos Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer



WORKDIR /app/Ejemplo



# Copiamos el archivo composer.json a la carpeta de trabajo
COPY ./composer.json /app/Ejemplo/composer.json

# Copiamos el archivo composer.lock a la carpeta de trabajo
COPY ./composer.lock /app/Ejemplo/composer.lock

# Copiamos los archivos de la aplicación de unicamente la carpeta Ejemplo a la carpeta de trabajo
COPY . .

# Instalamos las dependencias de Composer
RUN composer install

# Establecemos el directorio de trabajo




# Exponemos el puerto 8000 para el servidor de desarrollo de Laravel
EXPOSE 8000

# Ejecutamos el comando "php artisan serve" para iniciar el servidor de desarrollo de Laravel
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=8000"]
